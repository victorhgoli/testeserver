package org.acme;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

@Path("/hello")
public class ExampleResource {

	private final String connectionUrl = "jdbc:sqlserver://10.50.35.49:1433;database=WMSALEX;user=SANKHYA;password=testehmg332;trustServerCertificate=false;";
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response hello(@QueryParam("teste") String teste) {
		String strFile = readJsonFile();
		String newType = "area";
		Integer id = 0;
		
		JSONObject jsonObj = new JSONObject(strFile);
		JSONArray pages = jsonObj.getJSONObject("dashBoard").getJSONArray("pages");
		
		for (int i = 0; i < pages.length(); i++) {
			JSONObject page = pages.getJSONObject(i);
			
			if (page.getInt("id") == id) {
				page.getJSONArray("components").getJSONObject(0).getJSONObject("data").getJSONObject("chart").put("type", newType);
			}
		}
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return Response.ok().entity(jsonObj.toString()).build();
	}
	
	private String sqlServerTest() {
		ResultSet rset = null;
		try (Connection connection = DriverManager.getConnection(connectionUrl);) {
			Statement s = connection.createStatement();
			String select = "SELECT * FROM TGFPAR order by NOMEPARC";
			rset = s.executeQuery(select);
			
			String msg = "";
			while (rset.next()) {
				msg += rset.getString("NOMEPARC");
			}
			
			connection.setAutoCommit(false);
			
			Savepoint save1 = connection.setSavepoint();
			Statement upd = connection.createStatement();
			upd.executeUpdate("UPDATE TGFPAR SET NOMEPARC = 'TRANSPORTADORA11111' WHERE CODPARC = 17");
			
			connection.commit();
			//connection.rollback(save1);
			
			return msg;
		}
		
		// Handle any errors that may have occurred.
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return "deu ruim";
	}
	
	private String teste() {
		return "{\"hello\": \"This is a JSON response1\"}";
	}
	
	private String readJsonFile() {
		try {
			return new String(Files.readAllBytes(Paths.get("C:\\Users\\dione.martins\\Desktop\\serverquarkus\\code-with-quarkus\\src\\main\\java\\org\\acme\\teste.json")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "Não coisou";
	}
}